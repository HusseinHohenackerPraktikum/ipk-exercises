
//# anstatt Leerstelle bei include

#include <iostream>
#include <string>

int main(){

// ... "; " am Ende fehlt
	std::string output = "Typing is difficult";
// output != outut, "<" vor output fehlt, vor endl; "std::" fehlt
	std::cout << output <<std::endl;

	// "; " am ende vergessen
	int ret = 0;
	// ret definiert, keinen retv
	return ret;
}
