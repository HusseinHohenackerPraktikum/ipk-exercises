

#include <iostream>
#include <cmath>

//using namespace std;

int fiboRekursiv( int n ){
	
	if ( ( n== 1 ) || ( n == 0 ) ){
		
		//std::cout << " " << n << "  " << std::flush;
		
		return n;
	} else {		
		
		//std::cout << " " << fiboRekursiv ( n - 2 ) << "  " << std::flush;	
		
		return( fiboRekursiv( n - 1 ) + fiboRekursiv ( n - 2 ) );
	}
	
}




// int main(int argc, char** argv){
	
int main() {
	
	int i = 0;
	int n;	
	std::cout << " N = " ;
	std::cin >> n;

		if ( n > 40 ){
			
			std::cout << " fiboRekursiv    -1 " << std::endl;
			
		} else {
	
	while( 0 <= n){
		
				
		//std::cout << "  " <<	fiboRekursiv( n ) << " <- i: " << i << "   " << std::flush;	
		
		// i Anzahl der fiboRekursiv Aufrufen
		i++;
		n--;
				}
	
		}
	
	return 0;
}