

#include <iostream>

auto message()
{
	return "Hello world! - message()";
}

int main(int argc, char** argv)
{
	std::cout << "Hello world!" << std::endl;
	//std::cout << message() << std::endl;
	return 0;
}


/* 
Aufgabe1

c)
ohne -o <name>
mit ls Inhalt anschauen

./a.out kann man Code ausführen

d)
mit cat helloworld.cc

wird Code/GEstalt der Code ausgegeben

e)
grep FILE /usr/include/stdio.h
gibt aus das Verzeichnis mit allen " *FILE* ..."

mit -n
grep FILE /usr/include/stdio.h -n

gibt aus Zeilen-Nr. 847:..

f) + g)
grep "Hello world!" helloworld.cc

	return "Hello world! - message()";     -----    Rückgabewert von Methode message()
	std::cout << "Hello world!" << std::endl;     ---- Ausgabezeile std::cout mit "Hello world!"


Aufgabe2
xargs ... https://www.tecmint.com/xargs-command-examples/

a)
find  /usr/include -name "*.h" | xargs
Listet alle .h - Dateien in include-File auf

find  /usr/include -name "*.h" | xargs -l  Liste von .h-Dateien mit Line-Nr insgesamt

b)

find  /usr/include -name "*.h" | xargs grep "#include" | wc
   2793    6147  174568
find List.h-Datei | xargs grep setzt alle dateien in einen Befehl | wc zählt dateien, wörter, Buchstaben
zählt Zeilen, Wörter, Buchstaben von #include

find  /usr/include -name "*.h" | xargs grep "#include" | wc -l
2793  Line-Anzahl von .h-Dateien

find  /usr/include -name "*.h" | xargs echo grep "#include"

alle #include beinhaltende Dateien in einer Zeile ausgegeben
grep #include ...






*/


